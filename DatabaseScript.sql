CREATE TABLE users (UserId Integer, FirstName Varchar(50), LastName Varchar(50), Email varchar(100));

CREATE SEQUENCE s_users_id;

 CREATE OR REPLACE FUNCTION UsersFunction()
 RETURNS "trigger" AS
 $BODY$
 BEGIN
   New.UserId:=nextval('s_users_id');
   Return NEW;
 END;
 $BODY$
 LANGUAGE 'plpgsql' VOLATILE;

 CREATE TRIGGER UserIdTrigger
 BEFORE INSERT
 ON users
 FOR EACH ROW
 EXECUTE PROCEDURE UsersFunction();