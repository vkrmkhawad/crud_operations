package com.greatlearning.jdbc_crud_services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class CrudDAO {

	//method to register new user
	public void registerUser(Connection con, String firstName, String lastName, String emailID) throws SQLException {
		if (con != null && !con.isClosed()) {
			String str = "insert into users (FirstName,LastName,Email) values (?,?,?)";
			PreparedStatement preparedStatement = con.prepareStatement(str);
			preparedStatement.setString(1, firstName);
			preparedStatement.setString(2, lastName);
			preparedStatement.setString(3, emailID);
			int row = preparedStatement.executeUpdate();
			System.out.println(row +" User has been added");
		}

	}
	
	//method to display all users
	public void displayAllUsers(Statement stmt) throws SQLException {
		String str = "select userid,firstname,lastname,email from users";
		ResultSet rs = stmt.executeQuery(str);
		int rowCounter = 0;
		while (rs.next()) {
			int userID = rs.getInt("userid");
			String firstName = rs.getString("firstname");
			String lastName = rs.getString("lastname");
			String emailId = rs.getString("email");

			System.out.println("The Data =");
			System.out.println("userid = " + userID);
			System.out.println("firstName = " + firstName);
			System.out.println("lastName = " + lastName);
			System.out.println("emailId = " + emailId);
			++rowCounter;
		}
		System.out.println("Count of records: " + rowCounter);
	}

	//method to delete user
	public void deleteUser(Connection con, String firstName, String lastName) throws SQLException {
		if (con != null && !con.isClosed()) {
			String str = "delete from users where firstname=? and lastname=?";
			PreparedStatement preparedStatement = con.prepareStatement(str);
			preparedStatement.setString(1, firstName);
			preparedStatement.setString(2, lastName);
			int row = preparedStatement.executeUpdate();
			System.out.println(row +" User has been deleted");
		}
	}

	//method to update user information
	public void updateUser(Connection con, String emailId, String firstName, String lastName) throws SQLException {
		if (con != null && !con.isClosed()) {
			String str = "update users set email=? where firstname=? and lastname=?";
			PreparedStatement preparedStatement = con.prepareStatement(str);
			preparedStatement.setString(1, emailId);
			preparedStatement.setString(2, firstName);
			preparedStatement.setString(3, lastName);
			int row = preparedStatement.executeUpdate();
			System.out.println(row +" User has been updated");
		}
	}
}
