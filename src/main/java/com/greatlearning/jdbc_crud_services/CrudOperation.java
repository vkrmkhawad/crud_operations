package com.greatlearning.jdbc_crud_services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class CrudOperation {
	public static void main(String[] args) {
		System.out.println("!!!! Welcome to the user CRUD services !!!!");
		System.out.println("--------------------------------------------");
		performCrudOperation();
	}

	// method to call crud operations
	public static void performCrudOperation() {
		CrudDAO crudDao = new CrudDAO();
		Connection connection = null;
		Scanner sc = new Scanner(System.in);

		try {
			connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "PG@123");
			Statement stmt = connection.createStatement();
			int input;
			String firstName;
			String lastName;
			String emailID;
			do {
				System.out.println("Enter the operation you want to perform");
				System.out.println("1. Registration");
				System.out.println("2. Update");
				System.out.println("3. Display Data");
				System.out.println("4. Delete");
				System.out.println("0. Exit");
				input = sc.nextInt();
				boolean validateOtp = generateAndValidateOtp(sc, input);
				if (validateOtp) {
					switch (input) {
					case 1:
						System.out.println("Enter first name");
						firstName = sc.next();
						System.out.println("Enter last name");
						lastName = sc.next();
						System.out.println("Enter Email ID");
						emailID = sc.next();
						crudDao.registerUser(connection, firstName, lastName, emailID);
						break;
					case 2:
						System.out.println("Enter first name");
						firstName = sc.next();
						System.out.println("Enter last name");
						lastName = sc.next();
						System.out.println("Enter Email ID to be updated");
						emailID = sc.next();
						crudDao.updateUser(connection, emailID, firstName, lastName);
						break;
					case 3:
						crudDao.displayAllUsers(stmt);
						break;
					case 4:
						System.out.println("Enter first name");
						firstName = sc.next();
						System.out.println("Enter last name");
						lastName = sc.next();
						crudDao.deleteUser(connection, firstName, lastName);
						break;
					case 0:
						System.out.println("Exiting from the operation");
						break;
					default:
						System.out.println("Not a valid input");
						break;
					}
				}
			} while (input > 0);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			sc.close();
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	// method to generate and validate OTP
	public static boolean generateAndValidateOtp(Scanner sc, int input) {
		if (input == 0)
			return true;
		System.out.println("Enter OTP sent:");
		String getOtp = GenerateOtp.getOTP();
		SendSms.sendSms(getOtp, "8884594569");
		String otpReceived = sc.next();
		if (getOtp.equals(otpReceived))
			return true;
		else {
			System.out.println("Invalid OTP");
			return false;
		}

	}
}
