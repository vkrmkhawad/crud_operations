package com.greatlearning.jdbc_crud_services;

import java.util.Random;

public class GenerateOtp {

	static String getOTP() {

		// Using random method
		Random rndm_method = new Random();

		int[] num = new int[4];
		String otp = "";

		for (int i = 0; i < 4; i++) {
			// generating random number between 0-9
			num[i] = rndm_method.nextInt(10);
			otp += num[i];
		}

		return otp;
	}
}
